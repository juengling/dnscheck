# DNS Checker

**Purpose**

Retrieve and save ip address of the given URLs.
No analysis of the logged data so far.

Feel free to fork and add URLs to check. Pull requests and further ideas welcome.

**Why?**

Articles say, there are intentions to hold people back from web sites
by manipulating their DNS entries. This tiny little project shall push
your attention to this behaviour and help to prove if it is true or not,
and - if it is true - help to prove what has been modified.

Any ideas to spread the word are welcome, especially to de-centralize
the check and then concentrate the results again.