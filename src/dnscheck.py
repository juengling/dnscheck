﻿'''
Created on 29.08.2017

@author: Christoph Jüngling, https://www.juengling-edv.de
'''
from argparse import ArgumentParser
from configparser import ConfigParser
from datetime import datetime
import os.path
import socket
import sys
from tqdm._tqdm import tqdm


__APP_NAME__ = 'dnscheck'
__APP_VERSION__ = '1.0.0'

__RETURN_OK__ = 0


def main(argv):
    '''
    DNS Checker: retrieves and saves ip address of the given URLs
    '''

    args = parse_command_line(argv)

    if args.verbose >= 1:
        print('Config file: ' + args.config)

    if not os.path.isfile(args.config):
        create_default_config_file(args.config)
        print('File {} not found, created it.'.format(args.config))

    entries = get_urls(args.config)

    if args.verbose >= 1:
        print('Check {count} DNS entries.'.format(count=len(entries)))

    if args.verbose >= 2:
        for entry in entries:
            print('  - ' + entry['url'])

    addresses = get_addresses(entries)
    write_log(args.logfile, addresses)

    return __RETURN_OK__


def parse_command_line(arguments):
    '''
    Parse the command line arguments

    :param arguments: List of command line arguments without program name
    :return: Arguments object
    '''

    parser = ArgumentParser(prog=__APP_NAME__ + ' v' + __APP_VERSION__,
                            description=main.__doc__)

    parser.add_argument('--config', default='dnscheck.ini',
                        help='Specify configuration file (default = dnscheck.ini)')

    parser.add_argument('--logfile', default='dnscheck.log',
                        help='Specify log file (default = dnscheck.log)')

    parser.add_argument('-v', '--verbose',
                        action='count',
                        default=0,
                        help='Increase verbosity level')

    args = parser.parse_args(arguments)
    return args


def get_urls(configfile):
    '''
    Read urls and their latest timestamp from config file

    :param configfile: path/name of the config file
    :return: List of URLs
    '''

    config = ConfigParser()
    config.read(configfile)
    return [url for url in config['check']]


def get_addresses(entries):
    '''

    :param entries: List of URLs to be checked 
    :return: Dict of URLs with IP addresses
    '''
    result = {}

    for entry in tqdm(entries):
        url = entry.strip()

        try:
            ip = socket.gethostbyname(url)
        except socket.gaierror:
            result[url] = None
        else:
            result[url] = ip

    return result


def write_log(filename, addresses):
    '''
    Write/append log file with urls, found ip addresses and current time stamp

    :param addresses: Dict with urls and ip addresses
    '''
    timestamp = str(datetime.now())
    with open(filename, 'a') as logfile:
        for name, ip in addresses.items():
            logfile.write('{}, {}, {}\n'.format(name, ip, timestamp))


def create_default_config_file(filename):
    '''
    Create and store default config file. This can be used as a base for own modifications.
    Don't forget the equal sign at the end of each entry!

    :param filename: Path/name of config file
    '''
    content = '''[check]
irgendwo.de=
nirgendwo.de=
unknown.xy=
heise.de=
golem.de=
8sidor.se=
'''
    with open(filename, 'w') as cfgfile:
        cfgfile.write(content)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
